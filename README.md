UVOS libusbled
==============

This is a library to control the UVOS usbled pcb.  
license: GPL v3.0 or later

Installing
----------

Requirements:

* cmake
* libusb-1.0

Build instructions:

1. run 'cmake .'
2. run 'make'

To install run 'make install' as root.

For api documentation look at the comments uvosled.h
